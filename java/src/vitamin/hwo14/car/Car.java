package vitamin.hwo14.car;

import vitamin.hwo14.Connection;
import vitamin.hwo14.message.Message;
import vitamin.hwo14.message.Message.CarPosition;
import vitamin.hwo14.message.Message.TrackPiece;

import static vitamin.hwo14.message.Message.*;

public class Car {
    Connection connection;
    String name = "Captain Slow";
    String color;

    int tick;
    Race race;
    Track track;
    int piecesAmount;
    double totalLength;
    double[] cumulativePieceLength;
    double[] furtherStraight;
    int[] nextCurve, nextSwitch;
    double maxFurtherStraight;

    double a0, a1, fr;
    double xPrev, vPrev, aPrev, thPrev;
    double x, v, a, th;
    boolean turboAvailable;
    double turboFactor, availableTurboFactor;
    int lastProcessedSwitch;
    int crashes;
    boolean inGame = false;

    public Car(Connection connection) {
        this.connection = connection;
    }

    void init(@SuppressWarnings("hiding") Race race) {
        this.race = race;
        this.track = race.track;
        track.initialize();
        piecesAmount = track.pieces.length;
        cumulativePieceLength = new double[piecesAmount + 1];
        furtherStraight = new double[piecesAmount];
        nextCurve = new int[piecesAmount];
        nextSwitch = new int[piecesAmount];
        totalLength = 0;
        int k = 0;
        for (TrackPiece piece : track.pieces) {
            totalLength += piece.length(0);
            k++;
            cumulativePieceLength[k] = totalLength;
        }
        maxFurtherStraight = 1e-10;
        for (int i = 0; i < piecesAmount; i++) {
        	for (int j = 1; j < piecesAmount; j++) {
        		TrackPiece p = track.pieces[(i + j) % piecesAmount];
        		if (p.isSwitch()) {
        			nextSwitch[i] = (i + j) % piecesAmount;
        			break;
        		}
        	}
        	for (int j = 1; j < piecesAmount; j++) {
        		TrackPiece p = track.pieces[(i + j) % piecesAmount];
        		if (!p.isStraight()) {
        			nextCurve[i] = (i + j) % piecesAmount;
        			break;
        		}
        		furtherStraight[i] += p.length;
        	}
        	if (!track.pieces[i].isStraight()) {
        		furtherStraight[i] = 0;
        		continue;
        	}
        	furtherStraight[i] += track.pieces[i].length;
        	if (furtherStraight[i] > maxFurtherStraight) {
        		maxFurtherStraight = furtherStraight[i];
        	}
        }
        xPrev = Double.NaN;
        vPrev = Double.NaN;
        aPrev = Double.NaN;
        thPrev = Double.NaN;
        x = Double.NaN;
        v = Double.NaN;
        a = Double.NaN;
        th = Double.NaN;
        turboAvailable = false;
        turboFactor = 1;
        tick = -1;
        lastProcessedSwitch = -1;
        crashes = 0;
    }

    double limitOnCurve(TrackPiece piece, int startLineIndex) {
    	double neat = 0.66 - 0.06 * crashes - 0.04 * (turboFactor > 1 ? 1 : 0);
    	neat = Math.max(neat, 0.3);
//    	System.out.println(piece.radius + " " + piece.radiusOnLane(startLineIndex) + " " + piece.radiusMinOfAllLanes());
//    	return neat * Math.sqrt(piece.radiusMinOfAllLanes());
    	return neat * Math.sqrt(piece.radiusOnLane(startLineIndex));
    }

    double makeMove(CarPosition[] carPositions) {
        tick++;
        CarPosition mine = null;
        for (CarPosition carPosition : carPositions) {
            if (carPosition.id.color.equals(color)) {
                mine = carPosition;
            }
        }
        if (mine == null) {
        	throw new NullPointerException();
        }
//        double carAngle = mine.angle;
        int lapNumber = mine.piecePosition.lap;
        int pieceIndex = mine.piecePosition.pieceIndex;
        TrackPiece piece = track.pieces[pieceIndex];
        double inPieceDistance = mine.piecePosition.inPieceDistance;
        int startLineIndex = mine.piecePosition.lane.startLaneIndex;

        xPrev = x;
        vPrev = v;
        aPrev = a;
        thPrev = th;

        x = lapNumber * totalLength + cumulativePieceLength[pieceIndex] + inPieceDistance;
        v = x - xPrev;
        a = v - vPrev;
        if (tick <= 5) {
            if (!Double.isNaN(aPrev) && a0 == 0) {
                fr = -(a - aPrev) / aPrev;
                a0 = aPrev / (1 - fr);
                a1 = a / (1 - fr);
                System.out.println("a0 = " + a0 + ",\tfr = " + fr);
            }
            return 1;
        }

        if (!piece.isSwitch()) {
        	int ns = nextSwitch[pieceIndex];
        	int nc = nextCurve[ns];
        	if (lastProcessedSwitch != ns) {
        		if (track.pieces[nc].angle < 0) {
        			if (startLineIndex != 0) {
        	            if (verbose) System.out.println("<< switch left");
        				connection.sendSwitchLine(true);
        			}
        		} else {
        			if (startLineIndex != track.lanes.length - 1) {
        	            if (verbose) System.out.println("<< switch right");
        				connection.sendSwitchLine(false);
        			}
        		}
        		lastProcessedSwitch = ns;
        	}
        }

        double aPredict = a0 * thPrev - vPrev * fr;
        double vPredict = vPrev + aPredict;
        if (v > 0 && Math.abs(vPredict - v) > 1e-7) {
            if (tick <= 6) {
                a0 = a1;
            }
//            System.err.println("Bad prediction: " + tick);
//            System.err.println(aPredict + "\t" + a + "\t" + (aPredict - a));
//            System.err.println(vPredict + "\t" + v + "\t" + (vPredict - v));
        }
        double maxV = Double.POSITIVE_INFINITY;
        if (!piece.isStraight()) {
            maxV = limitOnCurve(piece, startLineIndex);
        }
        TrackPiece nextPiece = track.pieces[(pieceIndex + 1) % piecesAmount];
        double maxVthere = Double.POSITIVE_INFINITY;
        if (!nextPiece.isStraight()) {
            double toNextPiece = piece.length(startLineIndex) - inPieceDistance;
            maxVthere = limitOnCurve(nextPiece, startLineIndex);
            double t = fr * toNextPiece / v + 1;
            t = Math.log(t) / fr;
            double vt = v * Math.pow(1 - fr, t);
            if (vt > maxVthere) {
                return 0;
            }
        }

        if (v > maxV) {
            return 0;
        }
        if (turboAvailable) {
        	if (furtherStraight[pieceIndex] - inPieceDistance > 0.8 * maxFurtherStraight) {
       			return 2;
        	}
        }
        if (turboFactor > 1) {
            double toNextPiece = furtherStraight[pieceIndex] - inPieceDistance;
            maxVthere = limitOnCurve(track.pieces[nextCurve[pieceIndex]], startLineIndex);
            double t = fr * toNextPiece / v + 1;
            t = Math.log(t) / fr;
            double vt = v * Math.pow(1 - fr, t);
            if (vt > maxVthere) {
                return 0;
            }
        }
        return 1;
    }

    static final String[] knownTracks = new String[]{"keimola", "germany", "usa", "france"};
    boolean verbose = true;
    
    public void run() {
        connection.sendJoin(name);
//		connection.sendCreateRace(name, knownTracks[0], null, 1);
        System.out.println("<< Sent join");

        for (;;) {
            final Message in = connection.receive();
            if (in == null) break;
            if (verbose) System.out.println(">> " + in.msgType);

            if (in instanceof CarPositionsMessage) {
            	if (!inGame) {
            		if (verbose) System.out.println("<< ping");
            		connection.sendPing();
            		continue;
            	}
                th = makeMove(((CarPositionsMessage) in).data);
                if (th > 1) {
                	th = 1;
                	if (verbose) System.out.println("<< turbo");
                	connection.sendTurbo();
                	System.err.println("SENT TURBO");
                	turboAvailable = false;
                }
                if (verbose) System.out.println("<< throttle " + th);
               	connection.sendThrottle(th, in.gameTick);
                if (tick % 128 == 0) {
//                    connection.sendPing();
                    System.out.println("tick " + tick);
                }
                continue;
            }
            if (in instanceof TurboAvailableMessage) {
                turboAvailable = true;
                availableTurboFactor = ((TurboAvailableMessage) in).data.turboFactor;
				System.err.println("turboAvailable");
                continue;
            }
            if (in instanceof JoinMessage) {
                continue;
            }
            if (in instanceof GameInitMessage) {
                System.out.println("Received init");
                init(((GameInitMessage) in).data.race);
                continue;
            }
            if (in instanceof GameStartMessage) {
            	inGame = true;
            	th = 1;
                if (verbose) System.out.println("<< throttle " + th);
               	connection.sendThrottle(th, in.gameTick);
                continue;
            }
            if (in instanceof GameEndMessage) {
            	inGame = false;
            	continue;
            }
            if (in instanceof YourCarMessage) {
                color = ((YourCarMessage) in).data.color;
                System.out.println("We are " + color);
                continue;
            }
            if (in instanceof LapFinishedMessage) {
                continue;
            }
            if (in instanceof FinishMessage) {
                System.err.println("Finished ^_^");
                continue;
            }
            if (in instanceof TournamentEndMessage) {
                continue;
            }
            if (in instanceof CrashMessage) {
            	if (((CrashMessage) in).data.color.equals(color)) {
            		crashes++;
            		System.err.println("===== BABOOOMBA! =====");
            	}
                continue;
            }
            if (in instanceof SpawnMessage) {
            	System.err.println("SPAWN");
            	continue;
            }
            if (in instanceof TurboStartMessage) {
            	turboFactor = availableTurboFactor;
            	continue;
            }
            if (in instanceof TurboEndMessage) {
            	turboFactor = 1;
            	continue;
            }
            System.err.println(in.msgType);
            if (verbose) System.out.println("<< ping");
            connection.sendPing();
        }
    }
}
