package vitamin.hwo14;

import vitamin.hwo14.message.JsonProcessor;
import vitamin.hwo14.message.Message;

import java.io.*;
import java.net.Socket;

public class Connection implements Closeable {
	public final String key;
    public final Socket socket;
    public final BufferedReader br;
    public final PrintWriter out;
    public final JsonProcessor processor = new JsonProcessor();

    public Connection(String host, int port, String key) throws IOException {
    	this.key = key;
        //Proper resource handling is for the weak
        socket = new Socket(host, port);
        br = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
    }

    public void send(Message m) {
        out.println(processor.write(m));
        out.flush();
    }

    public void sendJoin(String name) {
        send(new Message.JoinMessage(new Message.JoinData(name, key)));
    }

    public void sendThrottle(double value, Integer gameTick) {
        send(new Message.ThrottleMessage(value, gameTick));
    }

    public void sendSwitchLine(boolean left) {
        send(new Message.SwitchLaneMessage(left ? "Left" : "Right"));
    }

    public void sendTurbo() {
        send(new Message.TurboMessage("DA RED GOES FASTA!!!"));
    }

    public void sendPing() {
        send(new Message.PingMessage());
    }

    public void sendCreateRace(String name, String trackName, String password, int carCount) {
        send(new Message.CreateRaceMessage(new Message.RaceData(new Message.JoinData(name, key), trackName, password, carCount)));
    }

    public void sendJoinRaceMessage(String name, String trackName, String password, int carCount) {
        send(new Message.JoinRaceMessage(new Message.RaceData(new Message.JoinData(name, key), trackName, password, carCount)));
    }

    public Message receive() {
        try {
            String line = br.readLine();
            if (line == null) return null;
            return processor.parse(line);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void close() throws IOException {
        out.close();
        br.close();
        socket.close();
    }
}
