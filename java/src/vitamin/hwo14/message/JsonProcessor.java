package vitamin.hwo14.message;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jetbrains.annotations.NotNull;

import static vitamin.hwo14.message.Message.*;

public class JsonProcessor {

    @NotNull private final Gson gson = new Gson();

    @NotNull public Message parse(String s) {
        try {
            final JsonParser parser = new JsonParser();
            final JsonObject root = parser.parse(s).getAsJsonObject();
            final String type = root.get("msgType").getAsString();
            final Class<? extends Message> clazz = TYPE_MAP.get(type);
            if (clazz == null) {
                log("Unknown type: " + type, s);
                return Message.UNKNOWN_MESSAGE;
            }
            return gson.fromJson(s, clazz);
        } catch (Throwable t) {
            log("Parsing error: " + t.getMessage(), s);
            return Message.UNKNOWN_MESSAGE;
        }
    }

    private static void log(String msg, String s) {
        System.err.println(msg);
        System.err.println("Content: \"" + s + "\"");
        System.err.println();
    }

    public String write(Message m) {
        if (m.msgType == null) {
            m.msgType = TYPE_MAP.inverse().get(m.getClass());
        }
        return gson.toJson(m);
    }
}
