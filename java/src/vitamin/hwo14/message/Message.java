package vitamin.hwo14.message;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

public class Message {
    public String msgType;
    public String gameId;
    public Integer gameTick;

    public static final Message UNKNOWN_MESSAGE = new Message() {{
        msgType = "unknown";
    }};

    @NotNull
    public static final BiMap<String, Class<? extends Message>> TYPE_MAP = ImmutableBiMap.<String, Class<? extends Message>>builder()
            .put("join", JoinMessage.class)
            .put("yourCar", YourCarMessage.class)
            .put("gameInit", GameInitMessage.class)
            .put("gameStart", GameStartMessage.class)
            .put("carPositions", CarPositionsMessage.class)
            .put("gameEnd", GameEndMessage.class)
            .put("tournamentEnd", TournamentEndMessage.class)
            .put("crash", CrashMessage.class)
            .put("spawn", SpawnMessage.class)
            .put("lapFinished", LapFinishedMessage.class)
            .put("dnf", DnfMessage.class)
            .put("finish", FinishMessage.class)
            .put("switchLane", SwitchLaneMessage.class)
            .put("turboAvailable", TurboAvailableMessage.class)
            .put("turbo", TurboMessage.class)
            .put("throttle", ThrottleMessage.class)
            .put("createRace", CreateRaceMessage.class)
            .put("joinRace", JoinRaceMessage.class)
            .put("ping", PingMessage.class)
            .put("turboEnd", TurboEndMessage.class)
            .put("turboStart", TurboStartMessage.class)
            .build();


    public static class JoinMessage extends Message {
        public JoinData data;

        public JoinMessage(JoinData data) {
            this.data = data;
        }
    }

    public static class JoinData {
        public String name;
        public String key;

        public JoinData(String name, String key) {
            this.name = name;
            this.key = key;
        }
    }

    public static class YourCarMessage extends Message {
        public CarDescription data;
    }

    public static class CarDescription {
        public String name;
        public String color;
    }

    public static class GameInitMessage extends Message {
        public GameInitData data;
    }

    public static class GameInitData {
        public Race race;
    }

    public static class Race {
        public Track track;
        public Car[] cars;
        public RaceSession raceSession;
    }

    public static class Track {
        public String id;
        public String name;
        public TrackPiece[] pieces;
        public LaneDescription[] lanes;
        public StartingPoint startingPoint;

        public void initialize() {
            for (TrackPiece piece : pieces) {
                piece.lanes = lanes;
            }
        }
    }

    public static class TrackPiece {
        public Double length;

        @SerializedName("switch")
        public Boolean doSwitch;

        public Double radius;
        public Double angle;

        public LaneDescription[] lanes;
        
        public boolean isStraight() {
        	return radius == null;
        }
        
        public boolean isSwitch() {
        	return Boolean.TRUE.equals(doSwitch);
        }

        public double length(int laneId) {
            if (length != null)
                return length;

            return Math.abs(angle) / 180 * Math.PI * radiusOnLane(laneId);
        }

		public double radiusOnLane(int laneId) {
			return radius - Math.signum(angle) * lanes[laneId].distanceFromCenter;
		}
		
		public double radiusMinOfAllLanes() {
			double r = Double.POSITIVE_INFINITY;
			for (int i = 0; i < lanes.length; i++) {
				r = Math.min(r, radiusOnLane(i));
			}
			return r;
		}
    }

    public static class LaneDescription {
        public Integer distanceFromCenter;
        public Integer index;
    }

    public static class StartingPoint {
        public Position position;
        public Double angle;
    }

    public static class Position {
        public Double x;
        public Double y;
    }

    public static class Car {
        public CarDescription id;
        public Dimensions dimensions;
    }

    public static class Dimensions {
        public Double length;
        public Double width;
        public Double guideFlagPosition;
    }

    public static class RaceSession {
        public Integer laps;
        public Integer maxLapTimeMs;
        public boolean quickRace;
    }

    public static class CarPositionsMessage extends Message {
        public CarPosition[] data;
    }

    public static class CarPosition {
        public CarDescription id;
        public Double angle;
        public PiecePosition piecePosition;
    }

    public static class Lane {
        public Integer startLaneIndex;
        public Integer endLaneIndex;
    }

    public static class PiecePosition {
        public Integer pieceIndex;
        public Double inPieceDistance;
        public Lane lane;
        public Integer lap;
    }

    public static class GameStartMessage extends Message {
    }

    public static class ThrottleMessage extends Message {
        public Double data;

        public ThrottleMessage(Double data, Integer gameTick) {
            this.data = data;
            this.gameTick = gameTick;
        }
    }

    public static class GameEndMessage extends Message {
        public GameEndData data;
    }

    public static class GameEndData {
        public Result[] results;
        public BestLap[] bestLaps;
    }

    public static class Result {
        public CarDescription car;
        public TotalResult result;
    }

    public static class TotalResult {
        public Integer laps;
        public Integer ticks;
        public Integer millis;
    }

    public static class BestLap {
        public CarDescription car;
        public LapResult result;
    }

    public static class LapResult {
        public Integer lap;
        public Integer ticks;
        public Integer millis;
    }

    public static class TournamentEndMessage extends Message {

    }

    public static class CrashMessage extends Message {
        public CarDescription data;
    }

    public static class SpawnMessage extends Message {
        public CarDescription data;
    }

    public static class LapFinishedMessage extends Message {
        public LapFinishedData data;
    }

    public static class LapFinishedData {
        public CarDescription car;
        public LapResult lapTime;
        public TotalResult raceTime;
        public Ranking ranking;
    }

    public static class Ranking {
        public Integer overall;
        public Integer fastestLap;
    }

    public static class DnfMessage extends Message {
        public DnfData data;
    }

    public static class DnfData {
        public CarDescription car;
        public String reason;
    }

    public static class FinishMessage extends Message {
        public CarDescription data;
    }

    public static class SwitchLaneMessage extends Message {
        public String data;

        public SwitchLaneMessage(String data) {
            this.data = data;
        }
    }

    public static class TurboAvailableMessage extends Message {
        public TurboData data;
    }

    public static class TurboData {
        public Double turboDurationMilliseconds;
        public Integer turboDurationTicks;
        public Double turboFactor;
    }

    public static class TurboMessage extends Message {
        public String data;

        public TurboMessage(String data) {
            this.data = data;
        }
    }

    public static class CreateRaceMessage extends Message {
        public RaceData data;

        public CreateRaceMessage(RaceData data) {
            this.data = data;
        }
    }

    public static class RaceData {
        public JoinData botId;
        public String trackName;
        public String password;
        public Integer carCount;

        public RaceData(JoinData botId, String trackName, String password, Integer carCount) {
            this.botId = botId;
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }
    }


    public static class JoinRaceMessage extends Message {
        public RaceData data;

        public JoinRaceMessage(RaceData data) {
            this.data = data;
        }
    }

    public static class PingMessage extends Message {
    }

    public static class TurboStartMessage extends Message {
        public CarDescription data;
    }

    public static class TurboEndMessage extends Message {
        public CarDescription data;
    }
}
