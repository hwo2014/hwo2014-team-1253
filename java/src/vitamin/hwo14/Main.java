package vitamin.hwo14;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
    	String key = "4Uu/Rj6+RuLtFA";
    	String server = "testserver.helloworldopen.com";
    	int port = 8091;
    	if (args.length > 0) {
    		server = args[0];
    		port = Integer.parseInt(args[1]);
    	}
        new Server(server, port, key).start();
    }
}
