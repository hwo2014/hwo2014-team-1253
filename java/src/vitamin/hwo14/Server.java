package vitamin.hwo14;

import org.jetbrains.annotations.NotNull;

import vitamin.hwo14.car.Car;

import java.io.*;

public class Server {
    @NotNull
    private final String host;
    private final int port;
    @NotNull
    private final String key;

    public Server(@NotNull String host, int port, @NotNull String key) {
        this.host = host;
        this.port = port;
        this.key = key;
    }

    public void start() throws IOException {
        try (final Connection connection = new Connection(host, port, key)) {
        	Car car = new Car(connection);
        	car.run();
        }
    }
}
