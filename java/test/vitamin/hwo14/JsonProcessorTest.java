package vitamin.hwo14;

import org.junit.Assert;
import vitamin.hwo14.message.JsonProcessor;
import vitamin.hwo14.message.Message;

public class JsonProcessorTest extends MessagesTest {

    private JsonProcessor processor = new JsonProcessor();

    @Override
    protected void doTest(String s, Class<? extends Message> messageClass) {
        Message m = processor.parse(s);
        Assert.assertEquals(messageClass, m.getClass());
        checkJsonEqual(s, processor.write(m));
    }
}
