package vitamin.hwo14;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Assert;
import org.junit.Test;
import vitamin.hwo14.message.Message;

public class MessagesTest {

    @Test public void joinTest() {
        doTest("{\"msgType\": \"join\", \"data\": {\n" +
                "  \"name\": \"Schumacher\",\n" +
                "  \"key\": \"UEWJBVNHDS\"\n" +
                "}}", Message.JoinMessage.class);
    }

    @Test public void yourCarTest() {
        doTest("{\"msgType\": \"yourCar\", \"data\": {\n" +
                "  \"name\": \"Schumacher\",\n" +
                "  \"color\": \"red\"\n" +
                "}}", Message.YourCarMessage.class);
    }

    @Test public void gameInitTest() {
        doTest("{\"msgType\": \"gameInit\", \"data\": {\n" +
                "  \"race\": {\n" +
                "    \"track\": {\n" +
                "      \"id\": \"indianapolis\",\n" +
                "      \"name\": \"Indianapolis\",\n" +
                "      \"pieces\": [\n" +
                "        {\n" +
                "          \"length\": 100.0\n" +
                "        },\n" +
                "        {\n" +
                "          \"length\": 100.0,\n" +
                "          \"switch\": true\n" +
                "        },\n" +
                "        {\n" +
                "          \"radius\": 200,\n" +
                "          \"angle\": 22.5\n" +
                "        }\n" +
                "      ],\n" +
                "      \"lanes\": [\n" +
                "        {\n" +
                "          \"distanceFromCenter\": -20,\n" +
                "          \"index\": 0\n" +
                "        },\n" +
                "        {\n" +
                "          \"distanceFromCenter\": 0,\n" +
                "          \"index\": 1\n" +
                "        },\n" +
                "        {\n" +
                "          \"distanceFromCenter\": 20,\n" +
                "          \"index\": 2\n" +
                "        }\n" +
                "      ],\n" +
                "      \"startingPoint\": {\n" +
                "        \"position\": {\n" +
                "          \"x\": -340.0,\n" +
                "          \"y\": -96.0\n" +
                "        },\n" +
                "        \"angle\": 90.0\n" +
                "      }\n" +
                "    },\n" +
                "    \"cars\": [\n" +
                "      {\n" +
                "        \"id\": {\n" +
                "          \"name\": \"Schumacher\",\n" +
                "          \"color\": \"red\"\n" +
                "        },\n" +
                "        \"dimensions\": {\n" +
                "          \"length\": 40.0,\n" +
                "          \"width\": 20.0,\n" +
                "          \"guideFlagPosition\": 10.0\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": {\n" +
                "          \"name\": \"Rosberg\",\n" +
                "          \"color\": \"blue\"\n" +
                "        },\n" +
                "        \"dimensions\": {\n" +
                "          \"length\": 40.0,\n" +
                "          \"width\": 20.0,\n" +
                "          \"guideFlagPosition\": 10.0\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"raceSession\": {\n" +
                "      \"laps\": 3,\n" +
                "      \"maxLapTimeMs\": 30000,\n" +
                "      \"quickRace\": true\n" +
                "    }\n" +
                "  }\n" +
                "}}", Message.GameInitMessage.class);
    }

    @Test public void gameStartTest() {
        doTest("{\"msgType\": \"gameStart\"}", Message.GameStartMessage.class);
    }

    @Test public void carPositionsTest() {
        doTest("{\"msgType\": \"carPositions\", \"data\": [\n" +
                "  {\n" +
                "    \"id\": {\n" +
                "      \"name\": \"Schumacher\",\n" +
                "      \"color\": \"red\"\n" +
                "    },\n" +
                "    \"angle\": 0.0,\n" +
                "    \"piecePosition\": {\n" +
                "      \"pieceIndex\": 0,\n" +
                "      \"inPieceDistance\": 0.0,\n" +
                "      \"lane\": {\n" +
                "        \"startLaneIndex\": 0,\n" +
                "        \"endLaneIndex\": 0\n" +
                "      },\n" +
                "      \"lap\": 0\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": {\n" +
                "      \"name\": \"Rosberg\",\n" +
                "      \"color\": \"blue\"\n" +
                "    },\n" +
                "    \"angle\": 45.0,\n" +
                "    \"piecePosition\": {\n" +
                "      \"pieceIndex\": 0,\n" +
                "      \"inPieceDistance\": 20.0,\n" +
                "      \"lane\": {\n" +
                "        \"startLaneIndex\": 1,\n" +
                "        \"endLaneIndex\": 1\n" +
                "      },\n" +
                "      \"lap\": 0\n" +
                "    }\n" +
                "  }\n" +
                "], \"gameId\": \"OIUHGERJWEOI\"}", Message.CarPositionsMessage.class);
    }

    @Test public void throttleTest() {
        doTest("{\"msgType\": \"throttle\", \"data\": 1.0}", Message.ThrottleMessage.class);
    }

    @Test public void gameEndTest() {
        doTest("{\"msgType\": \"gameEnd\", \"data\": {\n" +
                "  \"results\": [\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Schumacher\",\n" +
                "        \"color\": \"red\"\n" +
                "      },\n" +
                "      \"result\": {\n" +
                "        \"laps\": 3,\n" +
                "        \"ticks\": 9999,\n" +
                "        \"millis\": 45245\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Rosberg\",\n" +
                "        \"color\": \"blue\"\n" +
                "      },\n" +
                "      \"result\": {}\n" +
                "    }\n" +
                "  ],\n" +
                "  \"bestLaps\": [\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Schumacher\",\n" +
                "        \"color\": \"red\"\n" +
                "      },\n" +
                "      \"result\": {\n" +
                "        \"lap\": 2,\n" +
                "        \"ticks\": 3333,\n" +
                "        \"millis\": 20000\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Rosberg\",\n" +
                "        \"color\": \"blue\"\n" +
                "      },\n" +
                "      \"result\": {}\n" +
                "    }\n" +
                "  ]\n" +
                "}}", Message.GameEndMessage.class);
    }

    @Test public void tournamentEndMessage() {
        doTest("{\"msgType\": \"tournamentEnd\"}", Message.TournamentEndMessage.class);
    }

    @Test public void crashTest() {
        doTest("{\"msgType\": \"crash\", \"data\": {\n" +
                "  \"name\": \"Rosberg\",\n" +
                "  \"color\": \"blue\"\n" +
                "}, \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 3}", Message.CrashMessage.class);
    }

    @Test public void spawnTest() {
        doTest("{\"msgType\": \"spawn\", \"data\": {\n" +
                "  \"name\": \"Rosberg\",\n" +
                "  \"color\": \"blue\"\n" +
                "}, \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 150}", Message.SpawnMessage.class);
    }

    @Test public void lapFinishedMessage() {
        doTest("{\"msgType\": \"lapFinished\", \"data\": {\n" +
                "  \"car\": {\n" +
                "    \"name\": \"Schumacher\",\n" +
                "    \"color\": \"red\"\n" +
                "  },\n" +
                "  \"lapTime\": {\n" +
                "    \"lap\": 1,\n" +
                "    \"ticks\": 666,\n" +
                "    \"millis\": 6660\n" +
                "  },\n" +
                "  \"raceTime\": {\n" +
                "    \"laps\": 1,\n" +
                "    \"ticks\": 666,\n" +
                "    \"millis\": 6660\n" +
                "  },\n" +
                "  \"ranking\": {\n" +
                "    \"overall\": 1,\n" +
                "    \"fastestLap\": 1\n" +
                "  }\n" +
                "}, \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 300}", Message.LapFinishedMessage.class);
    }

    @Test public void dnfTest() {
        doTest("{\"msgType\": \"dnf\", \"data\": {\n" +
                "  \"car\": {\n" +
                "    \"name\": \"Rosberg\",\n" +
                "    \"color\": \"blue\"\n" +
                "  },\n" +
                "  \"reason\": \"disconnected\"\n" +
                "}, \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 650}", Message.DnfMessage.class);
    }

    @Test public void finishTest() {
        doTest("{\"msgType\": \"finish\", \"data\": {\n" +
                "  \"name\": \"Schumacher\",\n" +
                "  \"color\": \"red\"\n" +
                "}, \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 2345}", Message.FinishMessage.class);
    }

    @Test public void switchLaneTest() {
        doTest("{\"msgType\": \"switchLane\", \"data\": \"Left\"}", Message.SwitchLaneMessage.class);
    }

    @Test public void turboAvailableTest() {
        doTest("{\"msgType\": \"turboAvailable\", \"data\": {\n" +
                "  \"turboDurationMilliseconds\": 500.0,\n" +
                "  \"turboDurationTicks\": 30,\n" +
                "  \"turboFactor\": 3.0\n" +
                "}}", Message.TurboAvailableMessage.class);
    }

    @Test public void turboTest() {
        doTest("{\"msgType\": \"turbo\", \"data\": \"Pow pow pow pow pow, or your of personalized turbo vitamin.hwo14.message\"}", Message.TurboMessage.class);
    }

    @Test public void createRaceTest() {
        doTest("{\"msgType\": \"createRace\", \"data\": {\n" +
                "  \"botId\": {\n" +
                "    \"name\": \"schumacher\",\n" +
                "    \"key\": \"UEWJBVNHDS\"\n" +
                "  },\n" +
                "  \"trackName\": \"hockenheim\",\n" +
                "  \"password\": \"schumi4ever\",\n" +
                "  \"carCount\": 3\n" +
                "}}", Message.CreateRaceMessage.class);
    }

    @Test public void joinRaceTest() {
        doTest("{\"msgType\": \"joinRace\", \"data\": {\n" +
                "  \"botId\": {\n" +
                "    \"name\": \"keke\",\n" +
                "    \"key\": \"IVMNERKWEW\"\n" +
                "  },\n" +
                "  \"trackName\": \"hockenheim\",\n" +
                "  \"password\": \"schumi4ever\",\n" +
                "  \"carCount\": 3\n" +
                "}}", Message.JoinRaceMessage.class);
    }

    @Test public void joinRaceWithoutPasswordTest() {
        doTest("{\"msgType\": \"joinRace\", \"data\": {\n" +
                "  \"botId\": {\n" +
                "    \"name\": \"keke\",\n" +
                "    \"key\": \"IVMNERKWEW\"\n" +
                "  },\n" +
                "  \"carCount\": 3\n" +
                "}}", Message.JoinRaceMessage.class);
    }

    @Test public void pingTest() {
        doTest("{\"msgType\": \"ping\"}", Message.PingMessage.class);
    }

    protected void doTest(final String s, Class<? extends Message> messageClass) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Message message = gson.fromJson(s, messageClass);
        checkJsonEqual(s, gson.toJson(message));
    }

    @SuppressWarnings("static-method")
	protected void checkJsonEqual(final String expected, final String actual) {
        final JsonParser parser = new JsonParser();
        JsonElement expectedJson = parser.parse(expected);
        JsonElement actualJson = parser.parse(actual);
        Assert.assertEquals(expectedJson, actualJson);
    }

}
